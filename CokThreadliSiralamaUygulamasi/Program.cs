﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using CokThreadliSiralamaUygulamasi;

namespace CTSU
{

    class Program
    {
        static void Main(string[] args)
        {
            Degiskenler degisken = new Degiskenler();

            Random r = new Random();
            int uretilenSayiAdeti = 0;
            int uretilenSayi;
            int sira = 0;

            //Rastgele birbirinden farklı 1000 adet sayi oluşturuyoruz.
            while (uretilenSayiAdeti != 1000)
            {
                uretilenSayi = r.Next(1, 1001);
                while (sira != 1000)
                {
                    if (degisken.binlikdizi[sira] == uretilenSayi || degisken.binlikdizi[0] == uretilenSayi)
                    {
                        uretilenSayi = r.Next(1, 1001);
                        sira = 0;
                    }
                    else if (degisken.binlikdizi[sira] != uretilenSayi && uretilenSayiAdeti == sira)
                    {
                        degisken.binlikdizi[sira] = uretilenSayi;
                        uretilenSayiAdeti++;
                        sira = 0;
                        uretilenSayi = r.Next(1, 1001);
                        break;
                    }
                    sira++;
                }
            }

            for (int i = 0; i < 500; i++)
            {
                degisken.ilk500[i] = degisken.binlikdizi[i];
                degisken.son500[i] = degisken.binlikdizi[i + 500];
            }


            
            for (int i = 0; i < 1000; i++)
            {
                Console.WriteLine(i + 1 + ".Sayi:" + degisken.binlikdizi[i]);

            }
            Console.WriteLine("Ben ilk random atanan sayılarım.");
            Console.ReadLine();

            
            
            for (int i = 0; i < 500; i++)
            {
                Console.WriteLine(i + 1 + ".Sayi:" + degisken.ilk500[i]);

            }
            Console.WriteLine("Ben Sırasız ilk500'üm.");
            Console.ReadLine();

            
            for (int i = 0; i < 500; i++)
            {
                Console.WriteLine(i + 1 + ".Sayi:" + degisken.son500[i]);

            }
            Console.WriteLine("Ben Sırasız son500'üm.");
            Console.ReadLine();
            


            var sortingThread0 = new Thread(() => sorting0(degisken.ilk500));
            sortingThread0.Start();
            var sortingThread1 = new Thread(() => sorting1(degisken.son500));                     
            sortingThread1.Start();

            
            
            for (int i = 0; i < 500; i++)
            {
                Console.WriteLine(i + 1 + ".Sayi:" + degisken.ilk500[i]);

            }
            Console.WriteLine("Ben Sıralı ilk500'üm.");
            Console.ReadLine();
            
            for (int i = 0; i < 500; i++)
            {
                Console.WriteLine(i + 1 + ".Sayi:" + degisken.son500[i]);

            }
            Console.WriteLine("Ben Sıralı son500'üm");
            Console.ReadLine();
            

            var mergeThread = new Thread(() => merge(degisken.ilk500, degisken.son500));
            mergeThread.Start();
            Console.ReadLine();

            
        }

        
        
        static void sorting0(int[] ilk500)
        {

            Degiskenler d = new Degiskenler();
            int tarama;
            bool swapped = false;
            for (tarama = 0; tarama < 500; tarama++)
            {
                swapped = false;
                for (int i = 0; i < 500 - tarama - 1; i++)
                {
                    if (ilk500[i] > ilk500[i + 1])
                    {
                        int temp;
                        temp = ilk500[i];
                        ilk500[i] = ilk500[i + 1];
                        ilk500[i + 1] = temp;
                        swapped = true;
                    }
                }
                if (!swapped)
                    break;
            }

            for (int i = 0; i < 500; i++)
            {
                d.ilk500[i] = ilk500[i];
            }

        }

        static void sorting1(int[] son500)
        {

            Degiskenler d = new Degiskenler();

            int tarama;
            bool swapped = false;
            for (tarama = 0; tarama < 500; tarama++)
            {
                swapped = false;
                for (int i = 0; i < 500 - tarama - 1; i++)
                {
                    if (son500[i] > son500[i + 1])
                    {
                        int temp;
                        temp = son500[i];
                        son500[i] = son500[i + 1];
                        son500[i + 1] = temp;
                        swapped = true;
                    }
                }
                if (!swapped)
                    break;
            }

            for (int i = 0; i < 500; i++)
            {
                d.son500[i] = son500[i];
            }
        }

        static void merge(int[] ilk500, int[] son500)
        {
            Degiskenler d = new Degiskenler();


            for (int i = 0; i < 500; i++)
            {
                d.binlikdizi[i] = ilk500[i];
                d.binlikdizi[i + 500] = son500[i];
            }

            int tarama;
            bool swapped = false;
            for (tarama = 0; tarama < 1000; tarama++)
            {
                swapped = false;
                for (int i = 0; i < 1000 - tarama - 1; i++)
                {
                    if (d.binlikdizi[i] > d.binlikdizi[i + 1])
                    {
                        int temp;
                        temp = d.binlikdizi[i];
                        d.binlikdizi[i] = d.binlikdizi[i + 1];
                        d.binlikdizi[i + 1] = temp;
                        swapped = true;
                    }
                }
                if (!swapped)
                    break;
            }

            
            for (int i = 0; i < 1000; i++)
            {
                Console.WriteLine(i + 1 + ".Sayi:" + d.binlikdizi[i]);
            }
            Console.WriteLine("Ben 1000'lik sıralıyım.");

            string dosya_yolu = @"C:\Users\Harun\Desktop\son.txt";
            //İşlem yapacağımız dosyanın yolunu belirtiyoruz.
            FileStream fs = new FileStream(dosya_yolu, FileMode.OpenOrCreate, FileAccess.Write);
            //Bir file stream nesnesi oluşturuyoruz. 1.parametre dosya yolunu,
            //2.parametre dosya varsa açılacağını yoksa oluşturulacağını belirtir,
            //3.parametre dosyaya erişimin veri yazmak için olacağını gösterir.
            StreamWriter sw = new StreamWriter(fs);
            //Yazma işlemi için bir StreamWriter nesnesi oluşturduk.

            for (int i = 0; i < 1000; i++)
            {
                sw.WriteLine(i+1+".Sayi:\n"+d.binlikdizi[i]);
            }

            //Dosyaya ekleyeceğimiz iki satırlık yazıyı WriteLine() metodu ile yazacağız.
            sw.Flush();
            //Veriyi tampon bölgeden dosyaya aktardık.
            sw.Close();
            fs.Close();


        }
    }
}
